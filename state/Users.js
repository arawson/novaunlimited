/**
 * The user logon and permissions module.
 * @module users
 */
define( ['bluebird', 'db', 'config', 'node-uuid', 'model-server/model-server'],
function(Promise, db, config, uuid, model) {

    var ex = {};

    var Login = model.Login;

    ex.login = function (username, password) {
        var s = null;

        return db.get(Login.User.nameOf(username))
        .then(function (doc) {
            var user = Login.User.deserialize(doc);

            return user.tryLogin(password);
        }).then( function (session) {
            s = session;
            return db.put(session);
        }).then( function (result) {
            return Promise.resolve(s);
        });
    };

    ex.saveUser = function (user) {
        db.put(user);
    };

    ex.getUser = function (userName) {
        return db.get(Login.User.nameOf(userName)).then(function (doc) {
            return Promise.resolve(Login.User.deserialize(doc));
        });
    };

    ex.fromSession = function (sessionId) {
        return db.get(sessionId).then( function (doc) {
            var session = Login.Session.deserialize(doc);
            return db.get(session.userId);
        }).then( function (doc) {
            var user = Login.User.deserialize(doc);
            return Promise.resolve(user);
        });
    };

    ex.logout = function (sessionId) {
        //TODO is this really an acceptable way to screen for uuids?
        var u = Login.Session.nameOf(uuid.unparse(uuid.parse(sessionId)));

        db.get(u).then(function (doc) {
            return db.remove(doc);
        }).then(function (result) {
            console.log('attempted to delete session ', u, ' result was ',
                result);
        }).catch(function (error) {
            console.log('error deleting session ', u, ' result was ',
                error);
        });
    };

    /////////////////////////////////////////////////////////////////////
    //TODO: is it really okay to init the admin user like this?
    var initAdmin = function() {
        //do nothing if no admin account specified
        if (!config.has('Admin')) {
            return;
        }

        console.log('attempting to create admin user');
        //just put the admin, we dont deal with cleanup of old admin
        //accounts here, and if it already exists, it will just fail
        var adminName = Login.User.nameOf(config.get('Admin.username'));
        var adminPass = config.get('Admin.password');

        Login.User.newUser(adminName, adminPass, Login.User.levels.admin)
        .then(function (user) {
            return db.put(user);
        }).catch(function (err) {
            console.warn('error making admin user: ', err);
        });
    };
    initAdmin();

    return ex;
});

