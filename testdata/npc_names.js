
var csv = require('csv');
var fs = require('fs');

var names = exports.names = [];

exports.init = function(callback) {
    var parser = csv.parse({delimiter: ','});
    var rstream = fs.createReadStream('./testdata/2010_census_surnames.min.csv');

    parser.on('end', function() {
        callback(null, names);
        parser.end();
    });

    parser.on('data', function(chunk) {
        names.push(chunk[0]);
    });

    parser.on('error', function(error) {
        callback(error, null);
    });

    //PIPE THAT BAD BOY INTO THE PARSER!
    rstream.pipe(parser);

    parser.resume();
};

