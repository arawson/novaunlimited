
var csv = require('csv');
var fs = require('fs');

var stars = exports.stars = [];

/////////////////////////////////////////
//open a file

exports.init = function(callback) {
    var stars = [];

    var parser = csv.parse({
        delimiter: ',',
        columns: ['id', 'hip', 'name', 'magnitude', 'clazz', 'colorIndex', 'x', 'y', 'z', 'luminosity'],
        skip_empty_lines: true,
        ltrim: true,
        auto_parse: true
    });

    var rstream = fs.createReadStream('./testdata/hygdata_v3.min.csv');

    parser.on('end', function() {
        callback(null, stars);
        parser.end();
    });

    parser.on('data', function(star) {
        //use hip if no name available
        star.name = star.name || ('HIP ' + star.hip);

        //convert from parsec to ly
        star.x *= 3.26;
        star.y *= 3.26;
        star.z *= 3.26;

        stars.push(star);
    });

    parser.on('error', function(error) {
        callback(error);
    });

    rstream.pipe(parser);

    parser.resume();
};

