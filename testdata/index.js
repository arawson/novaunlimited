
var QuadTree = require('simple-quadtree');
var prettyjson = require('prettyjson');
var Promise = require('bluebird');

var stat = exports.stat = require('./stat.js');
var npc_names = exports.npc_names = Promise.promisifyAll(require('./npc_names.js'));
var star_data = exports.star_data = Promise.promisifyAll(require('./star_data.js'));

var logins = exports.logins = require('./logins.js');

exports.circleQuad = function(x, y, radius) {
    return {
        x: x - radius,
        y: y - radius,
        w: radius * 2,
        h: radius * 2
    };
};

//TODO: store this, its super important!v
//This is the time variable, where should it go?
exports.date = 0;

//TODO put these constants into a config file
//use exponential decay to model this toooooo
exports.Rumor.hardTimeLimit = 15;

//backcalculated from formula in RumorSum.prototype.feed
exports.RumorSum.prototype.rangeLimit = 0;

var planets = {
    earth: new exports.Planet('Earth', new exports.Cargo(250, 100, 10, 100), 7000),
    venus: new exports.Planet('Mars', new exports.Cargo(0, 200, 100, 30), 100),
    cal: new exports.Planet('Centauri A1', new exports.Cargo(100, 255, 120, 80), 1),
    cab1: new exports.Planet('Centauri B1', new exports.Cargo(100, 100, 100, 100), 1000),
    tce: new exports.Planet('Tau Ceti e', new exports.Cargo(50, 250, 150, 100), 100)
};

var defaultStars = {
    sol: new exports.Star("Sol", 0, 0, 'G2V', [planets.venus, planets.earth]),
    centauria: new exports.Star('Alpha Centauri', 4, 0, "G2V", [planets.ca1, planets.cab1]),
    barnad: new exports.Star('Barnard\'s Star', 5, 0, "M4Ve", []),
    luhman16: new exports.Star('Luhman 16', 7, 0, "L8", []),
    sirius: new exports.Star('Sirius', 9, 0, "A1V", []),
    tauceti: new exports.Star('Tau Ceti', 10, 0, "G8V", [planets.tce])
};

var galaxy = exports.galaxy = new Galaxy('test_galaxy' ,exports.GalaxySize.small);

var chars = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K',
             'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V',
             'W', 'X', 'Y', 'Z'];

var pickRand = function(list) {
    var i = parseInt(Math.random() * list.length);
    return list[i];
};

var randName = function() {
    return pickRand(chars) + '. ' + pickRand(npc_names.names);
};

var randNPC = function() {
    var n = randName();
    //console.log('npc: ', n);
    return new exports.NPC(n, null);
};

//galaxy.starNames.sol.putRumor(new exports.Rumor(exports.RumorType.boom, 100, 0));

//TODO: implement real jump limits for npcs
exports.jumpLimit = 15;

//random seeding of npcs
//this is where shit gets complicated
//this code can't be tested deterministically
//it only exists to create a good starting environment
//and since the rules are intended to create emergent behavior
//we get to test those too , weheeeee!

var starP = star_data.initAsync()
.then(function(stars) {
    console.log('read in ', stars.length, ' stars.');

    //stars suitable for insertion
    var s = [];
    for (var i = 0; i < stars.length; i++) {
        var star = stars[i];
        s.push(new exports.Star(
            star.name,
            star.x,
            star.y,
            star.clazz,
            [new exports.Planet(star.name + ' Alpha', new exports.Cargo(
                Math.random(), Math.random(), Math.random(), Math.random()), 1000)]
        ));
    }

    galaxy.putStars(s);
})
.catch(function(error) {
    console.log('error reading stars ', error);

    galaxy.putStars(defaultStars);
});

var nameP = npc_names.initAsync()
.then(function(names) {
    console.log('read in', names.length, ' names.');
})
.catch(function(error) {
    console.log('error reading names ', error);
});

Promise.all([nameP, starP])
.then(function() {
    console.log('names and stars read in');

    galaxy.stars.get(galaxy.size, function(star) {
        star.cantina.npcs.push(randNPC());
        star.cantina.npcs.push(randNPC());
        star.cantina.npcs.push(randNPC());

        return true;
    });
});

logins.addUser('arawson', 'badgers', function(error) {
    console.log(error || 'arawson added successfully');
});

/*
var PouchDB = require('pouchdb');

var db = new PouchDB('singleplayer', {});

db.allDocs().then(function (result) {
    console.log(prettyjson.render(result, {}));
});

db.put(galaxy).then(function (response) {
    console.log(response);
}).catch(function (err) {
    console.log(err);
});

var galaxian = require('./galaxy.js');
var g = new galaxian();
console.log(prettyjson.render(galaxian.GalaxySize));
*/

