
var bcrypt = require('bcrypt');
var uuid = require('node-uuid');
var moment = require('moment');

var constants = {
    pepper: 'slabs better written today',
    rounds: 8
};

var users = {};

var sessions = {};

var User = function(name, hash, rounds) {
    //user account can be logged into
    this.active = true;

    //the bcrypt hash of the user's password + salt
    this.hash = hash;

    this.rounds = rounds;
};

var Session = function(user) {
    //the user object of the session
    this.user = user;

    //this way we can reject old sessions
    this.session = null;

    //a nice uuid of the session
    this.id = uuid.v4();

    this.expires = moment().add(7, 'days');
};

exports.addUser = function(username, password, callback) {
    if (users[username]) {
        callback('user already exists');
    } else {
        bcrypt.hash(password + constants.pepper, constants.rounds, function(err, hash) {
            if (err) {
                callback(err);
            } else {
                users[username] = new User(username, hash, constants.rounds);
            }
        });
    }
};

var setSession = function(username) {
    var u = users[username];

    //expire old session forcibly
    //future gets will return undefined
    if (u.session) {
        delete sessions[u.session.id];
    }

    var s = new Session(u);
    sessions[s.id] = s;
    return s;
};

var getSession = exports.getSession = function(id) {
    //TODO check expiration dates
    return sessions[id];
};

exports.tryLogin = function(username, password, callback) {
    //TODO introduce rate limiting
    //TODO callback with a token
    if (users[username]) {

        if (users[username].active) {
            bcrypt.compare(password + constants.pepper, users[username].hash, function(err, res) {
                if (err) {
                    console.error(err);
                    callback(err);
                } else {
                    if (res) {
                        var s = setSession(username);
                        callback(null, s.id);
                    } else {
                        callback('Bad Password');
                    }
                }
            });

        } else {
            callback('account disabled');

        }

    } else {
        callback('no user called ' + username);
    }
};

