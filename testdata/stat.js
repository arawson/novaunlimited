
//crude transition and threshold functions

exports.sigmoid = function(t) {
    return 1.0 / ( 1 + Math.exp(-t) );
};

exports.step = function(t) {
    return (t >= 0) ? 1 : 0;
};

