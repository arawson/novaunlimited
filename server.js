#!/bin/env node

var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var minimist = require('minimist');
var util = require('./util.js');

app.use(express.static('./client'));
app.use('/model', express.static('./model'));
app.use(bodyParser.urlencoded({extended:false}));

var model = require('./testdata');
var commands = require('./commands');

app.post('/terminal', function(req, res) {
    console.log('post received on terminal');
    console.log('req.body is ', req.body);

    var line = req.body.command.split(' ');
    var cmdstring = line[0];
    var args = minimist(line.slice(1));

    //pick command from map
    var cmd = commands.map[cmdstring] || commands.map.unknown;
    args = util.scrubBlanks(args);
    var result = cmd.execute(args, model, req.body);

    res.send(JSON.stringify(result));
});


app.post('/login', function(req, res) {
    console.log('received login request for ', req.body.username);

    model.logins.tryLogin(req.body.username, req.body.password, function(error, result) {
        console.log('login err is ', error);
        console.log('login result is ', result);
        res.send({error: error, sessionId: result});
    });
});
////////////////////////////////////

var shutdown = false;

exports.onExit = function() {
    if (!shutdown) {
        //perform shutdown logic
        shutdown = true;
        console.log('shutting down server...');
        process.exit(0);
    }
};

exports.onLoad = function(win) {
    var isNodeWebkit = (win !== undefined);
    if (win) {
        win.on('close', exports.onExit);
    }

    var server = app.listen(3000, function() {
        var host = server.address().address;
        var port = server.address().port;

        console.log('server listening at http://%s:%s', host, port);
    });

    ///process.on('exit', exports.onExit);
    process.on('SIGINT', exports.onExit);
    //process.on('SIGTERM', exports.onExit);
    //process.on('SIGHUP', exports.onExit);
};

