
/**
 * The commands module. All interaction is performed through the CLI, and all
 * commands must be registered against the commands module.
 * @module commands
 */
define( ['minimist', './Help', './View', './Step'],
function(minimist, Help, View, Step) {
    var ex = {};

    var map = {};

    ex.addCmd = function () {
        for (var i = 0; i < arguments.length; i++) {
            var module = arguments[i];

            if (!map[module.cmdstr]) {
                map[module.cmdstr] = module;
            } else {
                throw 'Error: attempt to clobber command map.';
            }
        }
    };

    ex.addCmd(Help, View, Step);

    var unknown = {
        execute: function(args, model, body) {
            var result = {};
            result.cerr = 'Unknown command, maybe it was misspelled?';
            return result;
        }
    };

    ex.exec = function (command, user, state) {
        var line = command.split(' ');
        var cmdstring = line[0];
        var args = minimist(line.slice(1));

        var cmd = map[cmdstring] || uknown;
        args = scrubBlanks(args);

        var result = cmd.execute(args, model, req.body);

        return result;
    };

    //Get rid of "" from "-" in args from minimist
    var scrubBlanks = function(args) {
        if (args._) {
            args._ = args._.filter(function(t) {return t.length > 0;});
        }
        return args;
    };

    return ex;
});

