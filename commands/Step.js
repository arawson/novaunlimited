
define( [],
function () {
    var ex = {};

    ex.cmdstr = 'step';

    //not really a cellular automaton so much as a graph and set automaton
    //or as I call it, a chubby graph automaton
    //  the nodes of the graph are stuffed with automata that are free
    //  to traverse the graph

    ex.execute = function(args, model, body) {
        var galaxy = model.galaxy;

        console.log('==================================TURN====================================');

        //hell, its about time!
        model.date++;
        console.log('stardate: ', model.date);

        //var local = new model.RumorSum();
        //var distant = new model.RumorSum();

        var locals = {};

        //threshold functions
        //sigmoid - prefer one end smoothly
        //gaussian - prefer middle smoothly

        //glossary:
        //  local rumor: rumor from the star the npc is living in
        //  distant rumor: rumor from other stars

        //variables
        //  bounty - history value of bonuses to money for hunting pirates
        //  boom - rumours of good markets
        //      +(local boom rumors) -(local bust rumors) -(distant boom rumors)
        //  bust - rumors of bad markets
        //      +(local bust rumors) +(local threat)
        //  threat - how bad the pirate problem is
        //      -bounty
        //  safety - how well traders are protected from pirates

        //how does an npc decide what to do?
        //
        //what did it do last turn? it needs some stickiness so it doesnt flip around violently
        //
        //for each star
        galaxy.stars.get(galaxy.size, function(star) {
            //expire rumors
            //date is used to dim rumors
            star.cantina.rumors = star.cantina.rumors.filter(function(r) {
                return (r.startDate + model.Rumor.hardTimeLimit >= model.date);
            });

            //collect rumors
            locals[star.name] = {
                sum: new model.RumorSum(star.cantina.rumors, model.date),
                distant: new model.RumorSum(),
                jumps: {},
                queue: []
            };

            return true;
        });

        //collect distant rumor information
        //ironically set in locals.distant
        galaxy.stars.get(galaxy.size, function(star) {
            galaxy.stars.get(model.circleQuad(star.x, star.y, model.jumpLimit), function(star2) {
                if (star2 !== star) {
                    var local = locals[star.name];
                    var d = Math.sqrt(Math.pow(star.x - star2.x, 2) + Math.pow(star.y - star.y, 2));

                    if (d < model.jumpLimit) {
                        var s = locals[star2.name].sum.getScaled(d);
                        locals[star.name].distant.add(s);
                    }

                    //calculate jump costs
                    //  jumps just cost money but increase faster with more distance
                    //      cost = 1.5 ^ d + 10;
                    //      TODO: calculate jumps once
                    local.jumps[star2.name] = Math.pow(1.5, d) + 10;

                }
                return true;
            });

            return true;
        });


        //now that data has been collected, perform action on the npcs
        // //decide what to do: //  buy cargo //  sell cargo //  steal cargo
        //      -bounty +boom -cargo -safety + threat
        //  hunt pirate
        //      +bounty -cargo -boom +bust
        //  move to another star (traverse the graph)
        //whats the word in the cantina?
        //  sum up the boom/bust and safety/threat rumours
        //      this read from the set (local star) and nearby nodes on the graph (close stars)
        //      needs some maximum range
        //      has to be done per system
        //  threat * bountyFactor
        //  boom - safety
        //  bust + threat
        //
        ////////////////////////////////////////
        //collect npc actions
        galaxy.stars.get(galaxy.size, function(star) {
            var local = locals[star.name];
            for (var i = 0; i < star.cantina.npcs; i++) {
                var npc = npcs[i];

                var decision = npc.decide(local, star);

                local.queue.push(decision);
            }

            return true;
        });

        /////////////////////////////////////////////////
        //begin processing npc actions
        //
        //prio 1 : buy/sell
        galaxy.stars.get(galaxy.size, function(star) {
            var marketDelta = [];
            for (var i = 0; i < star.planets.length; i++) {
                marketDelta[i] = model.Goods(0, 0, 0, 0);
            }

            var queue = locals[star.name].queue;

            for (i = 0; i < queue; i++) {
                var action = queue[i];
                var npc = action.npc;
                switch (action.action) {
                    case model.NPC.actions.buy:
                        marketDelta[action.where][action.what] -= action.howmuch;
                        npc.money -= action.howmuch * star.planets[action.where].marketModifiers[action.what];
                    break;

                    case model.NPC.actions.sell:
                        marketDelta[action.where][action.what] += action.howmuch;
                        npc.money += action.howmuch * star.planets[action.where].marketModifiers[action.what];
                    break;
                }
            }

            //apply the npc effects to the market all at once
            for (i = 0; i < star.planets.length; i++) {
                var del = marketDelta[i];
                var planet = star.planets[i];

                for (var g in del) {
                    if (del[g] > 0) {
                        planet.marketModifiers[g]++;
                    }
                    if (del[g] < 0) {
                        planet.marketModifiers[g]--;
                    }

                    //apply the source/sink effects from the planets
                    //really just source
                    planet.marketModifiers[g] *= planet.marketStatic[g];
                }

            }

            //TODO generate boom/bust rumors

            //TODO apply faction market actions

            //filter out the ones that we processed on this pass
            locals[star.name].queue = locals[star.name].queue.filter(function(a) {
                return a.action != model.NPC.actions.buy && a.action != model.NPC.actions.sell;
            });

            return true;
        });

        //hunting lays a trap for pirates
        //  prepare for trap by pirate
        //stealing lays a trap for traders
        //  trap a random npc
        //      target has cargo? threaten
        //          target is hunter? fight
        //              keep track of win/loss for hunter
        //          target is not hunter? get some cargo
        //      else:
        //          nothing
        //      keep track of success/failure for hunter
        //  note: this is a hypergeometric distribution
        //
        //note: only these two activities directly generate rumors
        //
        //prio 2 : steal/hunt
        galaxy.stars.get(galaxy.size, function(star) {
            var queue = locals[star.name].queue;
            var payouts = 0;

            for (i = 0; i < star.cantina.npcs.length; i++) {
                star.cantina.npcs[i].hunter = false;
            }

            //mark hunters first
            for (i = 0; i < queue; i++) {
                var action = queue[i];
                var npc = action.npc;

                if (action.type === model.NPC.actions.hunt) {
                    npc.hunter = true;
                }
            }

            for (i = 0; i < queue; i++) {
                var action = queue[i];
                var npc = action.npc;

                if (action.type === model.NPC.actions.steal) {
                    //try to steal from a random npc
                    var selected = parseInt(Math.random() * star.cantina.npcs.length);

                    var victim = star.cantina.npcs[selected];

                    if (victim.hunter) {
                        //DO EPIC BATTLE!
                        //NOT REALLY, THE PIRATE JUST LOSES FOR NOW
                        //TODO generate a safety rumor
                        victim.money += star.cantina.bounty;
                        payouts++;
                    } else {
                        //determine if the victim has cargo
                        if (victim.cargo.isEmpty()) {
                            //victim has no cargo, do nothing for either
                        } else {
                            //victim has cargo
                            //take some cargo
                            npc.cargo.add(victim.cargo.takeSome());
                            //TODO generate a piracy rumor
                        }
                    }
                }
            }

            star.cantina.bounty /= payouts;

            //filter out the ones that we processed on this pass
            locals[star.name].queue = locals[star.name].queue.filter(function(a) {
                return a.action != model.NPC.actions.steal && a.action != model.NPC.actions.hunt;
            });

            return true;
        });

        //prio 3 : move between stars
        galaxy.stars.get(galaxy.size, function(star) {
            return true;
        });
    };

    return ex;

});

