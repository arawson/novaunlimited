
define( [],
function() {
    var ex = {};

    var map = {};

    ex.cmdstr = 'help';

    ex.execute = function(args, model, body) {
        console.log('call help!');
        var result = {};

        var catagory = args._[0];
        result.cout = map[catagory] || map.general;

        return result;
    };

    ex.unknown = 'Unrecognized command, maybe it was misspelled?';

    map = {
        general: 'Welcome to Nova Unlimited!\n' +
                 'Type "help <catagory>" with one of the following catagories for more information.\n' +
                 ' login: how to make an account or login\n' +
                 ' view: how to look at what\' happening in the galaxy\n' +
                 ' about: who made this thing?',

        login: 'Help unavailable at this time.',

        view: 'The command form is: "view <options> <category> <what>"\n' +
              '<options> may be nothing or:\n' +
              'No options at this time.\n' +
              '<category> is what kind of thing you want to view.\n' +
              'Catagor\n' +
              '<what> is what you want to view',

        about: 'Copyright Aaron Rawson 2015 under the MIT license.'
    };

    return ex;
});

