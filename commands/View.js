
define( [],
function () {

    //View command: for retrieving data about the galaxy

    var ex = {};

    ex.cmdstr = 'view';

    var views = {};

    ex.execute = function(args, model, body) {
        console.log('call view');
        console.log(JSON.stringify(args));

        var result = {};

        var category = args._[0];

        if (category) {
            var what = '';
            try {
                what = args._.slice(1).reduce(function(last, cur) {
                    return last + ' ' + cur;
                });
            } catch(error) {
            }
            console.log('what is ', what);
            if (what) {what = what.toLowerCase();}

            var viewfn = views[category];
            if (viewfn) {
                viewfn(args, model, body, what, result);
            } else {
                rresult.cerr = 'Unknown category: ' + category;
            }

        } else {
            result.cerr = "View what category?";
        }

        return result;
    };

    views.star = function(args, model, body, what, result) {

        console.log('search for exact star: ', what);

        if (what) {
            var star = model.galaxy.starNames[what];

            result.vector = {
                type: 'star',
                mode: 'pretty',
                name: what,
                star: star
            };

            if (!star) {
                result.cout = 'No star named: "' + what + '" was found.';
            } else {
                result.cout = 'Viewing star "' + star.name + '".';
            }
        } else {
            result.cerr = "View which star?";
        }

    };

    views.galaxy = function(args, model, body, what, result) {
        console.log('possibly the best galaxy in existence');

        result.vector = {
            type: 'galaxy',
            mode: 'pretty',
            galaxy: []
        };

        ///dont actually send the whole galaxy
        var g = result.vector.galaxy;

        model.galaxy.stars.get(model.galaxy.size, function(star) {
            g.push({
                name: star.name,
                planet_count: star.planets.length,
                x: star.x,
                y: star.y,
                clazz: star.clazz
            });

            return true;
        });

        result.cout = "Viewing the galaxy.";
    };

    return ex;

});

