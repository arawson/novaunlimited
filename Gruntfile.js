
module.exports = function(grunt) {

    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
        sass: {
            dist: {
                options: {
                    style: 'expanded'
                },
                files: {
                    'client/css/flicker.css': 'client/scss/flicker.scss',
                    'client/css/terminal.css': 'client/scss/terminal.scss',
                    'client/css/nu-bootstrap.css': 'client/scss/nu-bootstrap.scss',
                    'client/css/index.css': 'client/scss/index.scss'
                }
            }
        },

        watch: {
            css: {
                files: 'client/scss/*.scss',
                tasks: ['sass']
            },
        },

        nodemon: {
            dev: {
                script: 'runserver.js'
            }
        },

        //easy dev environment
        concurrent: {
            dev: ['watch', 'nodemon'],
            options: {
                logConcurrentOutput: true
            }
        }
    });

    //default task
    grunt.registerTask('default', ['concurrent:dev']);
};

