
define( ['./Star', './Faction', 'simple-quadtree'],
function (Star, Faction, QuadTree) {

    var Galaxy = function(id, name, size) {
        this._id = id;
        this.name = name;
        this.size = size;

        this.stars = QuadTree(size.x, size.y, size.w, size.h, {maxchildren:25});
        this.starList = [];
        this.factions = [];

        //removed because it is just a temporary value
        //not part of the data model
        //this.planetsFromStar = {}
        //this.starNames = {}
    };

    Galaxy.Size = {
        small: {name: 'Small', x: -10, y: -10, w: 20, h: 20},
        medium: {name: 'Medium', x: -50, y: -50, w: 100, h: 100}
    };

    Galaxy.prototype.putStars = function (starL) {
        for (var i = 0; i < starList.length; i++) {
            var star = starL[i];

            if (star.x < this.size.x || star.y < this.size.y ||
                star.x > this.size.w/2 || star.y > this.size.h/2) {
                console.warn('attempt to add star outside bounds of galaxy: ',
                 star.name);
            } else {
                console.log('add successful');
                console.log(star);
                this.stars.put(star);
                this.starList.push(star);
                //this.starNames[star.name.toLowerCase()] = star;
            }
        }
    };

    Galaxy.prototype.putFactions = function (factionList) {
        for (var i = 0; i < factionList.length; i++) {
            this.factions.push(factionList[i]);
        }
    };

    Galaxy.prototype.serialize = function () {
        return JSON.stringify(this);
    };

    Galaxy.deserialize = function(json) {
        var g = new Galaxy(json.name, json.size);

        var i;

        var s = [];
        for (i = 0; i < json.stars.length; i++) {
            s.push(Star.deserialize(json.stars[i]));
        }
        g.putStars(s);

        var f = [];
        for (i = 0; i < json.factions.length; i++) {
            f.push(Faction.deserialize(json.factions[i]));
        }
        g.putFactions(f);

        //TODO other finalization of the galaxy?

        return g;
    };

    return Galaxy;
});

