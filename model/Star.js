
define(['./Planet', './Rumor', './NPC'], function (Planet, Rumor, NPC) {

    var Star = function (id, name, x, y, clazz, planets) {
        this._id = id;

        this.name = name;
        this.x = x;
        this.y = y;
        //quadtree compat
        this.w = 0;
        this.h = 0;
        //
        this.planets = planets;
        this.clazz = clazz;
        this.cantina = {
            safety: 0,
            rumors: [],
            npcs: [],
            bounty: 0
        };
    };

    Star.prototype.putRumor = function(rumor) {
        this.cantina.rumors.push(rumor);
    };

    Star.prototype.serialize = function () {
        return JSON.stringify(this);
    };

    Star.deserialize = function (json) {
        var s = new Star(json.name, json.x, json.y, json.clazz, []);
        s._id = json._id;
        s.cantina.bounty = json.cantina.bounty;
        s.cantina.safety = json.cantina.safety;

        var i;

        for (i = 0; i < json.planets.length; i++) {
            s.planets.push(Planet.deserialize(json.planets[i]));
        }

        for (i = 0; i < json.cantina.npcs.length; i++) {
            s.cantina.npcs.push(NPC.deserialize(json.cantina.npcs[i]));
        }

        for (i = 0; i < json.cantina.rumors.length; i++) {
            s.cantina.rumors.push(Rumor.deserialize(json.cantina.rumors[i]));
        }

        return s;
    };

    return Star;

});

