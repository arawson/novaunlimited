
define( ['./Cargo'], function (Cargo) {

    var Planet = function(name, marketModifiers, population) {
        this.name = name;

        //currently a multiplier to goods transactions
        //so this number is proportional to scarcity
        //and inversely proportional to abundance
        this.marketModifiers = marketModifiers;

        this.marketStatic = marketModifiers.copy();

        this.population = parseInt(population, 10);
    };

    Planet.prototype.serialize = function () {
        return JSON.stringify(this);
    };

    Planet.deserialize = function (json) {
        var mods = Cargo.deserialize(json.marketModifiers);
        var stat = Cargo.deserialize(json.marketStatic);

        var p = new Planet(json.name, stat, json.population);
        p.marketModifiers = mods;

        return p;
    };

    return Planet;
});

