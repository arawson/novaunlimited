
define( ['./Cargo', './Faction', './Galaxy', './NPC', './Planet', './Rumor',
        './RumorSum', './Star'],
    function (Cargo, Faction, Galaxy, NPC, Planet, Rumor, RumorSum,
        Star) {

    /*
    var Cargo = require('./Cargo');
    var Faction = require('./Faction');
    var Galaxy = require('./Galaxy');
    var NPC = require('./NPC');
    var Planet = require('./Planet');
    var Rumor = require('./Rumor');
    var RumorSum = require('./RumorSum');
    var Star = require('./Star');
    */

    var ex = {
        Cargo: Cargo,
        Faction: Faction,
        Galaxy: Galaxy,
        NPC: NPC,
        Planet: Planet,
        Rumor: Rumor,
        RumorSum: RumorSum,
        Star: Star
    };

    return ex;
});

