
define( ['./Faction', './Cargo'], function (Faction, Cargo) {

    var NPC = function (name, faction) {
        this.name = name;
        this.money = 0;
        this.cargo = new exports.Goods(0, 0, 0, 0);
        this.spent = new exports.Goods(0, 0, 0, 0);
        this.career = null;
        this.faction = null;
    };

    NPC.Action = function(source, values) {
        this.source = source; //NPC
        this.action = values.action;
        this.what = values.what;
        this.where = values.where;
        this.howmuch = values.howmuch;
    };

    NPC.Action.TYPE = {
        buy: 'buy', //what where(planet) howmuch prio 1
        sell: 'sell', //what where(planet) howmuch prio 1
        steal: 'steal', //no params prio 2
        hunt: 'hunt', // no params prio 2
        move: 'move' //where prio 3
    };

    NPC.prototype.decide = function(local, star) {
        //TODO return some action!
    };

    NPC.prototype.serialize = function () {
        return JSON.stringify(this);
    };

    NPC.deserialize = function (json) {
        //TODO handle factions wheeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
        //boooooooooooooooooooooooo
        var n = new NPC(json.name, factions);

        n.cargo = Cargo.deserialize(json.cargo);
        n.spent = Cargo.deserialize(json.spent);

        n.career = json.career;
        n.faction = Faction.deserialize(json.faction);
    };

    return NPC;
});

