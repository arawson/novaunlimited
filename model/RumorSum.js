
/**
 * A module representing a sum of many rumors: used by NPCs directly to make
 * decisions.
 * @module model/RumorSum
 */
define( ['./Rumor'], function (Rumor) {

    var RumorType = Rumor.Type;

    /**
     * Represents the sum of many rumors: used by NPCs directly to make
     * decisions.
     * @alias module model/RumorSum
     * @constructor
     * @param {Array} rumors - A list of rumors to sum.
     * @param {Number} date - The current date.
     */
    var RumorSum = function(rumors, date) {
        this.values = {
            crash: 0,
            boom: 0,
            piracy: 0,
            safety: 0
        };

        if (rumors) {
            this.feed(rumors, date);
        }
    };

    /**
     * Add a single rumor to this sum.
     * @param {Rumor} rumor - The Rumor to add to this sum.
     */
    RumorSum.prototype.add = function(rumor, date) {
        var str = rumor.getStrength(date);
        this.values.crash  += str * rumor.values.crash;
        this.values.boom   += str * rumor.values.boom;
        this.values.piracy += str * rumor.values.piracy;
        this.values.safety += str * rumor.values.safety;
    };

    /**
     * Add many rumors to this sum.
     * @param {Array} rumors - An array of rumors to add to this sum.
     */
    RumorSum.prototype.feed = function(rumors, date) {
        for (var i = 0; i < rumors.length; i++) {
            var r = rumors[i];

            if (r.type === RumorType.crash) {
                this.values.crash += r.getStrength(date);

            } else if (r.type === RumorType.boom) {
                this.values.boom += r.getStrength(date);

            } else if (r.type === RumorType.piracy) {
                this.values.piracy += r.getStrength(date);

            } else if (r.type === RumorType.safety) {
                this.values.safety += r.getStrength(date);

            }
        }
    };

    /**
     * Get a copy of this sum, diminished by distance using a super secret
     * formula!
     * @param {Number} distance - The distance scale factor.
     * @returns {RumorSum} The scaled copy of this.
     */
    RumorSum.prototype.getScaled = function (distance) {
        var f = Math.pow(2, -0.5 * distance);

        var ret = new RumorSum();
        ret.values.boom = this.values.boom * f;
        ret.values.crash = this.values.crash * f;
        ret.values.piracy = this.values.piracy * f;
        ret.values.safety = this.values.safety * f;

        return ret;
    };

    RumorSum.prototype.serialize = function () {
        return JSON.stringify(this);
    };

    RumorSum.deserialize = function (json) {
        var r = new RumorSum();
        r.values.boom = json.values.boom;
        r.values.crash = json.values.crash;
        r.values.piracy = json.values.piracy;
        r.values.safety = json.values.safety;
        return r;
    };

    return RumorSum;

});

