
/**
 * A module representing a rumor.
 * @module model/Rumor
 */
define(function (require) {

    /**
     * Represents a rumor, a method of communication between NPCs. rumors
     * represent gossip between NPCs and diminish over time to allow new rumors
     * to take hold.
     * @alias module model/Rumor
     * @constructor
     * @param {Object} type - The type of the rumor, from the rumor.Type object.
     * @param {Number} credibility - The initial strength of the rumor.
     * @param {Number} startDate - The stardate the rumor began.
     */
    var Rumor = function (type, credibility, startDate) {
        this.type = type;
        this.credibility = credibility;
        this.startDate = startDate;
    };

    /**
     * The type of a rumor. Rumors fall into 2 categories: market rumors, and
     * safety rumors. Crash and boom are rumors of what direction a market is
     * trending and affects trade decisions of NPCs. Safety and piracy rumors
     * indicate how dangerous a system is, which affects NPC movement and the
     * decisions of pirates and bounty hunters.
     */
    Rumor.Type = {
        crash: 'market crash',
        boom: 'market boom',
        piracy: 'piracy rising',
        safety: 'piracy declining'
    };

    /**
     * Get the strength of this rumor at the date specified.
     * @param {Number} date - The current date.
     */
    Rumor.getStrength = function(date) {
        return this.credibility * Math.pow(2, 0.5 * (this.startDate - date));
    };

    /**
     * Serialize a rumor to JSON. rumors are directly translated to and from
     * JSON, unlike some other objects.
     * @returns {JSON String} The serialized rumor.
     */
    Rumor.prototype.serialize = function() {
        return JSON.stringify(this);
    };

    /**
     * Construct a rumor from a JSON object. rumors are directly translated to
     * and from JSON, unlike some other objects.
     * @param {JSON Object} json - The JSON object to deserialize.
     * @returns {rumor} The deserialized rumor.
     */
    Rumor.deserialize = function(json) {
        return new rumor(json.type, json.credibility, json.startDate);
    };

    return Rumor;

});

