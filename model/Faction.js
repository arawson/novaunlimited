
define(function (require) {

    var Star = require('./Star');

    var Faction = function (name, homeId, type, act1, act2) {
        this.name = name;
        this.homeId = homeId;
        this.type = type;
        this.act1 = act1;
        this.act2 = act2;
    };

    Faction.Activity = {
        none: 'none',
        farming: 'farming',
        mining: 'mining',
        industry: 'industry',
        defense: 'defense',
        piracy: 'piracy'
    };

    Faction.Type = {
        democracy: 'democracy',
        corporation: 'corporation',
        pirate: 'pirate'
    };

    Faction.prototype.serialize = function () {
        return JSON.stringify(this);
    };

    Faction.deserialize = function (json) {
        var f = new Faction(json.name, json.homeId, json.type,
            json.act1, json.act2);

        return f;
    };

    return Faction;
});

