
/**
 * A module representing the cargo that NPCs and players carry as well as
 * transactions with the local markets.
 * @module model/Cargo
 */
define(function () {

    /**
     * Represents cargo carried by NPCs and players as well as transactions with
     * the local markets.
     * @alias module model/Cargo
     * @constructor
     * @param {Number} bio - The amount of food and other organics.
     * @param {Number} metal - The amount of metals.
     * @param {Number} fuel - The amount of fuel.
     * @param {Number} goods - The amount of manufactured goods.
     */
    var Cargo = function (bio, metal, fuel, goods) {
        this.bio = parseInt(bio, 10) || 0;
        this.metal = parseInt(metal, 10) || 0;
        this.fuel = parseInt(fuel, 10) || 0;
        this.goods = parseInt(goods, 10) || 0;
    };

    Cargo.TYPES = {
        bio: 'bio',
        metal: 'metal',
        fuel: 'fuel',
        goods: 'goods'
    };

    Cargo.TYPES_LIST = [
        Cargo.TYPES.bio,
        Cargo.TYPES.metal,
        Cargo.TYPES.fuel,
        Cargo.TYPES.goods
    ];

    Cargo.prototype.copy = function() {
        return new exports.Goods(this.bio, this.metal, this.fuel, this.goods);
    };

    Cargo.prototype.isEmpty = function() {
        return this.bio === 0 && this.metal === 0 && this.fuel === 0 &&
            this.goods === 0;
    };

    Cargo.prototype.add = function (cargo) {
        this.metal += cargo.metal;
        this.bio += cargo.bio;
        this.fuel += cargo.fuel;
        this.goods += cargo.goods;
    };

    Cargo.prototype.takeSome = function() {
        //take up to 5 units of cargo
        var ret = new exports.Goods();

        for (var i = 0; i < 5; i++) {
            var choose = parseInt(Math.random() * 4);
            var name = exports.Cargo.typesList[choose];
            if (this[name] > 0) {
                this[name]--;
                ret[name]++;
            }
        }

        return ret;
    };

    Cargo.prototype.serialize = function() {
        return JSON.stringify(this);
    };

    Cargo.deserialize = function(json) {
        return new Cargo(json.bio, json.metal, json.fuel, json.goods);
    };

    return Cargo;

});

