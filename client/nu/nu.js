
define( ['model/model', './login'],
function (model, login) {

    var nu = {};

    nu.login = login;

    nu.handlers = {
        //This is an example of a static handler
        example: {
            whatever: "data you want",
            handle: function(data, status) {
                //data is just plain old return from the server's post
                if (data.cout) {console.log("cout=", data.cout);}
                if (data.cerr) {console.log("cerr=", data.cerr);}
            }
        },
    };

    nu.intercepts = {
        login: {
            //duh, just unhide the login, hide everything else
            exec: function(args) {
                //kinda specific and hacky :(
                nu.handlers.term.term.disable();
                $('#logincover').show();
                $('#inputUsername').focus();
            }
        }
    };

    nu.sendCommand = function(command, override) {
        var parts = command.split(' ');
        var cmd = parts[0];

        var to = override || '/terminal';

        if (nu.intercepts[cmd]) {
            nu.intercepts[cmd].exec(parts.slice(1));

        } else {
            var session = $.cookie('sessionId');

            var data = {
                command: command,
                sessionId: session
            };

            $.post(to, data,
                function(data, status, jqxhr) {
                    for (var k in nu.handlers) {
                        nu.handlers[k].handle(data, status, jqxhr);
                    }
                },
            'json');
        }
    };

    return nu;

});

