
define(function (require) {

    var nu = require('./nu');

    //this guy is an object-based handler
    var TermHandler = function() {
    };

    TermHandler.prototype.termHook = function(command, term) {
        this.term = term;
        if (command !== '') {
            try {
                nu.sendCommand(command);
            } catch(e) {
                term.error(e);
            }
        } else {
            term.echo('');
        }
    };

    TermHandler.prototype.init = function(element, termopts, outerthis) {
        element.terminal(function(command, term) {
            outerthis.termHook(command, term);
        }, termopts);
    };

    TermHandler.prototype.handle = function(data, status) {
        if (this.term) {
            if (data.cout) {this.term.echo(data.cout);}
            if (data.cerr) {this.term.error(data.cerr);}
        }
    };

    return TermHandler;

});

