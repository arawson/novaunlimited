
define( ['three', 'requestAnimFrame', 'stats', 'OrbitControls', './nu'],
function(THREE, requestAnimFrame, Stats, OrbitControls, nu) {

    var GraphicsHandler = function (element) {
        console.log(element);
        console.log('WxH = ', element.clientWidth, 'x', element.clientHeight);
        var scene = this.scene = new THREE.Scene();
        var camera = this.camera = new THREE.PerspectiveCamera(
            75, element.clientWidth / element.clientHeight , 0.1, 1000);

        var renderer = this.renderer = new THREE.WebGLRenderer();
        renderer.setSize(element.clientWidth, element.clientHeight);

        var stats = this.stats = new Stats();

        var controls = this.controls = new THREE.OrbitControls( camera,
            renderer.domElement );
        controls.damping = 0.2;

        element.appendChild(stats.domElement);
        element.appendChild(renderer.domElement);

        //listen to window to handle resizing
        window.addEventListener('resize', function () {
            renderer.setSize(0,0);
            renderer.setSize( element.clientWidth, element.clientHeight);
            camera.aspect = element.clientWidth / element.clientHeight;
            camera.updateProjectionMatrix();
        }, false);

        var geometry = new THREE.BoxGeometry( 1, 1, 1 );
        var material = new THREE.MeshBasicMaterial( { color: 0x00ff00 } );
        var cube = new THREE.Mesh( geometry, material );
        scene.add( cube );

        camera.position.z = 5;

    };

    GraphicsHandler.prototype.render = function() {
        var that = this;
        requestAnimFrame( function() {that.render();});

        this.controls.update();
        this.renderer.render( this.scene, this.camera );
        this.stats.update();
    };

    return GraphicsHandler;

});

