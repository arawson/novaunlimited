
define( ['jquery'],
function ($) {

    var login = function (elements) {
        this.buttons = elements.buttons;
        this.ui = elements.ui;
        this.input = elements.input;
        this.isLoggedIn = false;

        var that = this;
        this.buttons.login.click(function () {
            that.ui.cover.toggle();
        });

        this.buttons.logout.click(function () {
            that.logout();
            return false;
        });

        this.buttons.submit.click(function () {
            that.tryLogin();

            return false;
        });

        this.ui.cover.hide();
        this.buttons.logout.hide();

        this.checkSession();
    };

    login.prototype.checkSession = function () {
        var sessionId = $.cookie('sessionId');
        var username = $.cookie('username');

        if (sessionId && username) {
            this.ui.username.text(username);
            this.buttons.logout.show();
            this.buttons.login.hide();
        }
    };

    login.prototype.logout = function() {
        console.log('sessionId: ', JSON.stringify($.cookie('sessionId')));
        $.post('/logout', {sessionId: $.cookie('sessionId')},
            function (data, status, jqxhr) {});

        //$.cookie('sessionId', undefined);
        $.removeCookie('sessionId');
        //$.cookie('username', null);
        $.removeCookie('username');

        this.buttons.login.show();
        this.buttons.logout.hide();
        this.ui.username.text("Not Logged In");
    };

    login.prototype.tryLogin = function () {
        var that = this;

        $.post('/login', {
            username: this.input.username.val(),
            password: this.input.password.val()},

            function (data, status, jqxhr) {
                console.log(JSON.stringify(data));

                that.loginReturn(data);
        });
    };

    login.prototype.loginReturn = function (data) {
        //TODO allow hooks for other parts to respond to login
        if (data.error) {
            //TODO show error better
            alert('Login failed.');
        }

        if (data.sessionId) {
            console.log(JSON.stringify(data.sessionId));
            $.cookie('sessionId', data.sessionId.uuid);
            $.cookie('username', data.username);

            this.buttons.login.hide();
            this.buttons.logout.show();
            this.ui.cover.hide();
            this.ui.username.text(data.username);
        } else {
            this.buttons.login.show();
            this.buttons.logout.hide();
            this.ui.username.text("Not Logged In");
        }
    };

    return login;

});

