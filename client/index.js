
require.config({
    baseUrl: 'js',

    paths: {
        //module id: path to the jquery file without js
        jquery: 'jquery',
        jquery_cookie: 'jquery.cookie',
        jquery_terminal: 'jquery.terminal',
        requestAnimFrame: 'RequestAnimFrame',
        threeCore: 'three.min',
        three: 'three',
        TrackballControls: 'controls/TrackballControls',
        OrbitControls: 'controls/OrbitControls',
        stats: 'stats.min',
        domReady: 'domReady',
        bootstrap: 'bootstrap',
        nu: '../nu',
        model: '../model',
        'simple-quadtree': 'qtree/qtree'
    },

    shim: {
        'jquery_terminal': {
            deps: ['jquery']
        },

        'bootstrap': {
            deps: ['jquery']
        },

        'threeCore': {
            exports: 'THREE'
        },

        'TrackballControls': {
            deps: ['threeCore'],
            exports: 'THREE'
        },

        'OrbitControls': {
            deps: ['threeCore'],
            exports: 'THREE'
        },

        'stats': {
            exports: 'Stats'
        }
    }
});

require(['jquery', 'jquery_terminal', 'jquery_cookie', 'domReady',
        'bootstrap', 'nu/nu', 'nu/TermHandler', 'nu/GraphicsHandler'],
function($, terminal, cookie, domReady, bootstrap, nu, TermHandler,
GraphicsHandler) {

    domReady(function() {

        var login = new nu.login({
            buttons: {
                logout: $('#logoutbutton'),
                login: $('#loginbutton'),
                submit: $('#loginsubmit')
            },
            ui: {
                cover: $('#logincover'),
                username: $('#username')
            },
            input: {
                username: $('#inputUsername'),
                password: $('#inputPassword')
            },
        });

        /*
        nu.handlers.term = new TermHandler();

        nu.handlers.term.init(
            $('#user_input'),
            {
                greetings: 'Welcome! Type "help" for help!',
                name: 'commandPrompt',
                prompt: '>'
            },
            nu.handlers.term);
        */

        var graphics = nu.handlers.graphics = new GraphicsHandler(
            document.getElementById('main'));
        graphics.render();

        //TODO logo of some sort
    });
});

