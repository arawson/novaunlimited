
define( ['config', 'node-uuid', 'moment', 'bcrypt', 'bluebird'],
function (config, uuid, moment, bcrypt, Promise) {

    Promise.promisifyAll(bcrypt);
    console.log('load user model module');

    var constants = {
        pepper: 'slabs better written today',
        //TODO make rounds configurable
        rounds: 8,
        //TODO make login timeout configurable
        login_days: config.get('Security.login_days')
    };

    var User = function(id, hash, rounds, level) {
        this._id = id;

        //TODO allow new users to not be active by default
        //user account can be used at all
        this.active = true;

        //the bcrypt hash of the user's password
        this.hash = hash;

        //the work factor given to bcrypt
        this.rounds = rounds;

        //set the permission level
        this.permission = level || User.levels.user;
    };

    User.levels = {
        admin: 'admin',
        user: 'user'
    };

    User.nameOf = function(name) {
        return 'user/' + name;
    };

    var Session = function(id, issued, userId) {
        this._id = Session.nameOf(id);

        this.uuid = id;

        this.issued = issued;

        this.userId = userId;
    };

    Session.nameOf = function(id) {
        return 'session/' + id;
    };

    //TODO use this for translations or something
    User.LoginErrors = {
        BAD_LOGIN: 'BAD_LOGIN',
        DISABLED: 'DISABLED'
    };

    /**
     * Create a user object from a username and password.
     * @param {string} username
     * @param {string} password
     * @param {User.levels} level - (optional) the permission level
     * @return {Promise<User>} A promise of the new User
     */
    User.newUser = function (username, password, level) {
        return bcrypt.hashAsync(password + constants.pepper, constants.rounds)
        .then(function (hash) {
            return Promise.resolve(new User(username, hash, constants.rounds,
            level));
        });
    };

    /**
     * Called to start a session when password-based login is successful.
     */
    User.prototype.initSession = function () {
        return new Session(uuid.v4(), moment(), this._id);
    };

    /**
     * Attempt to login with password.
     * @param {string} password - The user password.
     * @return {Promise<sid>} the session id if the login was successful.
     */
    User.prototype.tryLogin = function (password, callback) {
        if (this.active) {
            var that = this;

            return bcrypt.compareAsync(password + constants.pepper, this.hash)
            .then(function (result) {
                console.log('!#$!@#$!@#$ current user is ', that);
                return Promise.resolve(that.initSession());
            });

        } else {
            return Promise.reject('user login failed');
        }
    };

    /**
     * Attempt to determine if the session is still valid.
     */
    Session.prototype.isValid = function() {
        return moment().isBefore(moment(this.issued).add(
            constants.login_days, 'days'));
    };

    Session.deserialize = function(json) {
        var s = new Session(json._id, json.issued, json.userId);
        s._rev = json._rev;
        return s;
    };

    User.deserialize = function (json) {
        var u = new User(json._id, json.hash, json.rounds);
        u.active = json.active;
        u.session = json.session;
        u._rev = json._rev;
        return u;
    };

    return {
        User: User,
        Session: Session
    };
});

