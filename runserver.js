#!/usr/bin/env node

var requirejs = require('requirejs');

requirejs.config({
    //pass the node require function to requirejs
    nodeRequire: require,

    baseUrl: '.',

    paths: {
        model: 'model',
        'model-server': 'model-server',
        commands: 'commands',
        db: './db',
        server: 'server2'
    },
});

requirejs(['server'],
function (server) {
    server.onLoad();
});

