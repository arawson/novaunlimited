
/**
 * The database module. It provides a pouchdb object from the server config.
 */
define( ['pouchdb', 'config', 'conditional'],
function(PouchDB, config, preconditions) {
    console.log('init database');
    ////////////////
    //turn on debugging first
    if (config.get('PouchDB.debug')) {
        PouchDB.debug.enable('*');
    }

    ///////////////////////////////////////////////////////////////////////////
    //load db connection info from configuration
    var options = {
        ajax: {}
    };

    if (config.has('PouchDB.username')) {
        preconditions.checkState(config.has('PouchDB.password'),
            'database authentication password missing from config');

        options.ajax.username = config.get('PouchDB.username');
        options.ajax.password = config.get('PouchDB.password');
    }

    options.name = config.get('PouchDB.url');

    var db = new PouchDB(options);


    return db;
});

