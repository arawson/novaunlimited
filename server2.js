
/**
 * The server module. It puts everything together.
 * @module server
 */
define( ['config', 'express', 'body-parser', 'commands/commands',
'state/state.js'],
function (config, express, bodyParser, cmd, state) {
    //has the server been shutdown?
    var shutdown = false;
    //the application variable provided by express
    var app = express();

    ///////////////////////////////////////////////////////////////////////////
    //setup express middleware
    //client files
    app.use(express.static('./client'));
    //make model js available for client
    app.use('/model', express.static('./model'));
    //middleware that parses json for express
    app.use(bodyParser.urlencoded({extended:false}));

    ///////////////////////////////////////////////////////////////////////////
    //express handlers

    // /terminal is where the client sends its commands
    app.post('/terminal', function (req, res) {
        state.users.fromSession(req.body.sessionId)
        .then(function (user) {

            result = cmd.exec(req.body.command, user, state);

            res.send(JSON.stringify(result));
        });
    });

    // /login is where login requests are sent
    // they are handled seperately for convenience's sake
    app.post('/login', function (req, res) {
        console.log('received login request for ', req.body.username);

        var username = req.body.username;
        var password = req.body.password;
        var ret = {};

        state.users.login(username, password)
        .then(function (sid) {
            if (sid) {
                ret.sessionId = sid;
                ret.username = username;
            } else {
                ret.error = true;
            }

            res.send(ret);
        })
        .catch(function (err) {
            console.error('error on login request: ' + err);
            res.send({error: true});
        });
    });

    app.post('/logout', function(req, res) {
        var sessionId = req.body.sessionId;
        console.log('sessionid is ', sessionId);
        if (sessionId) {
            state.users.logout(sessionId);
        }
    });

    var ex = {};

    ex.onLoad = function(win) {
        var isNWJS = (win !== undefined);
        if (isNWJS) {
            win.on('close', ex.onExit);
        }

        var server = app.listen(config.get('Server.port'), function() {
            var host = server.address().address;
            var port = server.address().port;

            console.log('server listening at http://%s:%s', host, port);
        });
    };

    ex.onExit = function() {
        if (!shutdown) {
            shutdown = true;
            console.log('shutting down server...');
            process.exit(0);
        }
    };

    return ex;
});

