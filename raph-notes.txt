here i'm collecting my notes on RaphaelJs's usage

rotation should use the transform , um form
    element.animate({transform: 'r360', 2000, 'bounce'})
        this is a bouncing rotation (CW) that that takes a total of 2 seconds

    element.attr({transform: 'r30'})
        this rotates an element by 20 degrees clockwise

rotation arguments
    2, 3, and 4 arg forms dont work as expected
        expected rotation about object
        received rotation about origin

translation in transform doesnt work as expected
    expected rotation about object
    received rotation about origin

